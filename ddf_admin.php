<?php
/*
Plugin Name: DDF Engine 
Plugin URI: https://bitbucket.org/ggregoire/
Description: Plugin to acces mls listing
Contributors: Dago Agacino
Version: 0.3.3.5
Author: Firstlook Media.
Author URI: www.myfirstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) */

/**
 * Create the function to output the contents of our Dashboard Widget.
 */

function ddf_dashboard_widget_function() {
/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
}
add_action( 'tester','ddf_test_connect' );

 function ddf_test_connect(){	
	$options=get_option('fl_general');
	$status='';
	//return $message=$this->getConnectString().$this->getQueryParams().'<br>';;
		try {	
		$url_string='https://###.###.###.###/';
		$ch = curl_init($url_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
		curl_setopt($ch, CURLAUTH_BASIC,CURLOPT_HTTPAUTH);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_USERPWD, '*********:********');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);
		$data = curl_exec($ch);
		//$contents = utf8_encode($data); 
		$curl_errno = curl_errno($ch);
		$curl_error = curl_error($ch);
		curl_close($ch);
		   
		if ($curl_errno > 0) {
			
			echo '<br><span style="color:red">DDF Not Connected..</span>';
			echo '<br><span style="color:red">HTTP Error : '.$curl_error.'</span>';
			
			$status=false;
		} else {	
			
			$agentsListings=json_decode($data,true, 512);	
			$status=true;
		}
	}
	catch( Exception $e ) {
		print_r('<br>'.$e);
	}
	
	return $status;
}

function my_search_form( $form ) {
	wp_enqueue_style( 'search style', plugins_url() .'/FL_DDF_Engine/css/style.css' );
	$form =  include(WP_PLUGIN_DIR.'/FL_DDF_Engine/ddf_master_search_form.php');
	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );
 
function admin_ddf_style() {
	 wp_enqueue_style( 'ddf-admin-box-style', plugins_url().'/FL_DDF_Engine/css/ddf_admin_style.css' );

 }

add_action( 'admin_enqueue_scripts','admin_ddf_style' );
add_action( 'admin_menu', 'fl_ddf_add_menu');
add_action('admin_init', 'fl_ddf_admin_init');
add_action('user_admin_menu', 'fl_ddf_add_menu');

//***********************************************************************


add_action( 'admin_bar_menu', 'toolbar_ddf_indicator', 999 );

function toolbar_ddf_indicator( $wp_admin_bar ) {
	$args = array(
		'id'    => 'fms_ddf_status',
		'title' => '<span style="color:'.get_ddf_plugin_status().';font-weight: bolder;">DDF</span>',
		'href'  => '#',
		'meta'  => array( 'class' => 'dashicons dashicons-admin-site' )
	);
	$wp_admin_bar->add_node( $args );
}

function get_ddf_plugin_status(){
$plugin_status='';
    if(ddf_test_connect()) {
		$plugin_status='green';
	}else{
		$plugin_status='gray';
	}
	return $plugin_status;
}

function fl_ddf_add_menu() {
	if (!current_user_can( 'manage_options' ) )  {
		wp_die( __( 'not sufficient permissions to access  page.' ) );
	}
	add_menu_page('DDF Settings', 'DDF Settings', 'manage_options', 'fl_ddf_menu', '','dashicons-admin-site',6);
	add_submenu_page( 'fl_ddf_menu', 'Search Documentation', 'DDF Documentation', 'manage_options', 'fl_ddf_simple', 'fl_ddf_simple');
}

function fl_ddf_analytics() {include('ddf_analytics.php');}
function fl_ddf_simple() {include( WP_PLUGIN_DIR.'/FL_DDF_Engine/index.html');}

function fl_ddf_admin_init(){init_ddf_settings();}
function init_ddf_settings() {register_setting('lf_plugin_ddf-group', 'ddf_settings.php');} 

//Use the fallowing short code withing wordpress page [ddf-search]
function ddf_search_short(){
	 include(WP_PLUGIN_DIR.'/FL_DDF_Engine/components/searchShort-code.php');
}

function ddf_search_form() {
	 include_once(WP_PLUGIN_DIR.'/FL_DDF_Engine/ddf_master.php');	
 }
 add_filter( 'get_search_form', 'ddf_search_form' ); 
 add_shortcode( 'firstLook_ddf','ddf_search_short' );

//***********************CLASESS***************************************
//***********************************************************************


if( is_admin() ){
   $ddf_settings = new DDF_SETTINGS();
}	

//*************************************************************************
//*************************************************************************
	
function ddf_styler_regiter( $wp_customize ) {
	$wp_customize->add_section('ddf_section', array(
					  'title' => __( 'DDF Property Card' ),
					  'description' => __( 'Custom CSS' ),
					  'panel' => '', // Not typically needed.
					  'priority' => 11,
					 'capability' => 'manage_options',
					  'theme_supports' => ''));

	$wp_customize->add_setting('ddf_title_background', array(
					  'type' => 'option', // or 'option'
					  'capability' => 'manage_options',
					  'default' => '#222222',
					  'transport' => 'postMessage', // or postMessage
					  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_title_font_color', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#fff',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_title_text_size', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '10',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_title_font', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '‘Metrophobic’, Arial, serif; font-weight: 400;',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_third_background', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#222222',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_third_font_color', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#fff',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_third_text_size', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '10',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_third_font', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '‘Metrophobic’, Arial, serif; font-weight: 400;',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));


	$wp_customize->add_setting('ddf_price_background', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#eaeaea',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_price_font_color', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#fff',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_price_text_size', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '10',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_price_font', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '‘Metrophobic’, Arial, serif; font-weight: 400;',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_button_background', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#5b5b5b',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_button_background_hover', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#5b5b5b',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_button_font_color', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '#fff',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_hex_color'));

	$wp_customize->add_setting('ddf_button_text_size', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '10',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_button_font', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '‘Metrophobic’, Arial, serif; font-weight: 400;',
				  'transport' => 'postMessage', // or postMessage
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('ddf_card_padding', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '1',
				  'transport' => 'postMessage', // or postMessage refresh
				  'sanitize_callback' => 'sanitize_text_field'));

	$wp_customize->add_setting('fms_listing_margin_left', array(
				  'type' => 'option', // or 'option'
				  'capability' => 'manage_options',
				  'default' => '1',
				  'transport' => 'postMessage', // or postMessage refresh
				  'sanitize_callback' => 'sanitize_text_field'));


	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'card-title-background_color', array(
					'label'        => __( 'Title Background Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_title_background')));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ddf_title_font_color',
				array('label'        => __( 'Title Text Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_title_font_color'))); 

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-title-text-size',
					array('label'          => __( 'Title Text Size (px)', 'ddf_theme' ),
						'section'        => 'ddf_section',
						'settings'       => 'ddf_title_text_size',
						'type'           => 'text')));  
	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-title-font',
					array('label'          => __( 'Title Font', 'ddf_theme' ),
						'section'        => 'ddf_section',
						'settings'       => 'ddf_title_font',
						'type'           => 'text')));  

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'card-third-background_color', array(
					'label'        => __( 'Third Background Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_third_background')));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ddf_third_font_color',
				array('label'        => __( 'Third Text Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_third_font_color'))); 

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-third-text-size',
					array('label'          => __( 'Third Text Size (px)', 'ddf__theme' ),
						'section'        => 'ddf_section',
						'settings'       => 'ddf_third_text_size',
						'type'           => 'text')));  
	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-third-font',
					array('label'          => __( 'Third Font', 'ddf_theme' ),
						'section'        => 'ddf_section',
						'settings'       => 'ddf_third_font',
						'type'           => 'text')));  

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'card-price-background_color', array(
					'label'        => __( 'Price Background Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_price_background')));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ddf_price_font_color', array(
					'label'        => __( 'Price Text Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_price_font_color'))); 

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'card-price-text-size', array(
					'label'        => __( 'Price Text Size (px)', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_price_text_size',
					'type'           => 'text')));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-price-font',
					array('label'          => __( 'Price Font', 'ddf_theme' ),
						'section'        => 'ddf_section',
						'settings'       => 'ddf_price_font',
						'type'           => 'text')));  

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'card-button-background-color', array(
					'label'        => __( 'Buttons background Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_button_background')));

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'card-button-background-hover-color', array(
					'label'        => __( 'Buttons hover Color', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_button_background_hover')));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'card-button-font_color', array(
						'label'        => __( 'Buttons text color', 'ddf_theme' ),
						'section'    => 'ddf_section',
						'settings'   => 'ddf_button_font_color')));

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'card-button-text-size', array(
					'label'        => __( 'Buttons text size (px)', 'ddf_theme' ),
					'section'    => 'ddf_section',
					'settings'   => 'ddf_button_text_size',
					'type'           => 'text')));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-button-font',
				array('label'          => __( 'Buttons font (see product documentation for detail)', 'ddf_theme' ),
					'section'        => 'ddf_section',
					'settings'       => 'ddf_button_font',
					'type'           => 'text')));  	

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'card-pading',
				array('label'          => __( 'Property Card Padding (%)', 'ddf_theme' ),
					'section'        => 'ddf_section',
					'settings'       => 'ddf_card_padding',
					'type'           => 'text')));  
			
		}
	
add_action( 'customize_register', 'ddf_styler_regiter' );

function ddf_custom_css_output() {
	if (! is_customize_preview()) {
		
	    echo '<style type="text/css" id="custom-theme-css">' .
	    get_theme_mod( 'custom_theme_css', '' ) . '</style>';
	    echo '<style type="text/css" id="custom-plugin-css">' .
	    get_option( 'custom_plugin_css', '' ) . '</style>';
	    echo '<style type="text/css" id="ddf-plugin-css">
	    .background-color:'.get_option('ddf_button_background','').'}
	    .prop-card-button:hover{background-color:'.get_option('ddf_button_background_hover','').'}
	    .prop-card-button{color:'.get_option( 'ddf_button_font_color','').'}
	    .prop-card-button{font-size:'.get_option( 'ddf_button_text_size','').'px}
	    .prop-card-button{font-family:'.get_option( 'ddf_button_font','').'}
	    .fl-ddf-primary-color{background-color:' .get_option( 'ddf_title_background', '' ) . '}
	    .fl-ddf-primary-color{color:'.get_option( 'ddf_title_font_color','').'}
	    .fl-ddf-primary-color{font-size:' .get_option( 'ddf_title_text_size', '' ) . 'px}
	    .fl-ddf-primary-color{font-family:'.get_option( 'ddf_title_font','').'}
	    .fl-ddf-seco-color{background-color:' .get_option( 'ddf_price_background', '' ) . '}
	    .fl-ddf-seco-color{color:'.get_option( 'ddf_price_font_color','').';}
	    .fl-ddf-seco-color{font-size:' .get_option( 'ddf_price_text_size', '' ) . 'px}
	    .fl-ddf-seco-color{font-family:'.get_option( 'ddf_price_font','').'}
	    .fl-ddf-third-color{background-color:' .get_option( 'ddf_third_background', '' ) . '}
	    .fl-ddf-third-color{color:'.get_option( 'ddf_third_font_color','').'}
	    .fl-ddf-third-color{font-size:' .get_option( 'ddf_third_text_size', '' ) . 'px}
	    .fl-ddf-third-color{font-family:'.get_option( 'ddf_third_font','').'}
	    .fl-property-card{padding:' .get_option( 'ddf_card_padding', '' ) . '%}
	   
	    </style>';
	
	}else{
		
		
	    echo '<style type="text/css" id="custom-theme-css">' .
	    get_theme_mod( 'custom_theme_css', '' ) . '</style>';
	    echo '<style type="text/css" id="custom-plugin-css">' .
	    get_option( 'custom_plugin_css', '' ) . '</style>';
	    echo '<style type="text/css" id="ddf-plugin-css">
	    .prop-card-button{background-color:'.get_option( 'ddf_button_background','').'}
	    .prop-card-button:hover{background-color:'.get_option( 'ddf_button_background_hover','').';}
	    .prop-card-button{color:'.get_option( 'ddf_button_font_color','').';}
	    .prop-card-button{font-size:'.get_option( 'ddf_button_text_size','').'px;}
	    .prop-card-button{font-family:'.get_option( 'ddf_button_font','').'}
	    .fl-ddf-primary-color{background-color:' .get_option( 'ddf_title_background', '' ) . ';}
	    .fl-ddf-primary-color{color:'.get_option( 'ddf_title_font_color','').';}
	    .fl-ddf-primary-color{font-size:' .get_option( 'ddf_title_text_size', '' ) . 'px;}
	    .fl-ddf-primary-color{font-family:'.get_option( 'ddf_title_font','').';}
	    .fl-ddf-seco-color{background-color:' .get_option( 'ddf_price_background', '' ) . ';}
	    .fl-ddf-seco-color{color:'.get_option( 'ddf_price_font_color','').';}
	    .fl-ddf-seco-color{font-size:' .get_option( 'ddf_price_text_size', '' ) . 'px;}
	    .fl-ddf-seco-color{font-family:'.get_option( 'ddf_price_font','').';}
	    .fl-ddf-third-color{background-color:' .get_option( 'ddf_third_background', '' ) . ';}
	    .fl-ddf-third-color{color:'.get_option( 'ddf_third_font_color','').';}
	    .fl-ddf-third-color{font-size:' .get_option( 'ddf_third_text_size', '' ) . 'px;}
	    .fl-ddf-third-color{font-family:'.get_option( 'ddf_third_font','').';}
	    .fl-property-card{padding:' .get_option( 'ddf_card_padding', '' ) . '%;}
	  
	    </style>';
	}
 
}

add_action( 'wp_head', 'ddf_custom_css_output');

function ddf_customize_preview_js() {
	wp_enqueue_script( 'ddf-customizer', plugins_url().'/FL_DDF_Engine/js/ddf_theme_customizer.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'ddf-customizer', plugins_url().'/FL_DDF_Engine/js/fl_ddf_app.js', array( 'jquery' ), '20150330', true );
}
add_action( 'customize_preview_init', 'ddf_customize_preview_js' );


?>