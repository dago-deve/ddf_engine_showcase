<?php
/* Base class for ddf data access configuration.
Contributors: Dago Agacino
Version: 2.0.0
Author: Firstlook Media.
Author URI: www.myfirstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) */	
include('ddf_accessor.php');
 function ddf_connect(){
	 $fl_wp_options=get_option('fl_general');

	 if(!empty ($_GET['province'])){$session_province=$_GET['province'];}else{$session_province=$fl_wp_options['province'];}
	 if(!empty ($_GET['city'])){$session_city=$_GET['city'];}else{$session_city=$fl_wp_options['city'];}
	 if(!empty ($_GET['agent_id'])){$session_agent_id=$_GET['agent_id'];}else{$session_agent_id=$fl_wp_options['agent_id'];}
	 if(!empty ($_GET['brokerage_id'])){$session_brokerage_id=$_GET['brokerage_id'];}else{$session_brokerage_id=$fl_wp_options['brokerage_id'];}
	 if(!empty ($_GET['min_price'])){$session_min_price=$_GET['min_price'];}else{$session_min_price=$fl_wp_options['min_price'];}
	 if(!empty ($_GET['max_price'])){$session_max_price=$_GET['max_price'];}else{$session_max_price=$fl_wp_options['max_price'];}
	 if(!empty ($_GET['min_baths'])){$session_min_baths=$_GET['min_baths'];}else{$session_min_baths=$fl_wp_options['min_baths'];}
	 if(!empty ($_GET['min_beds'])){$session_min_beds=$_GET['min_beds'];}else{$session_min_beds=$fl_wp_options['min_beds'];}
	 if(!empty($_GET['neighborhood'])){$session_neighborhood=$_GET['neighborhood'];}else{$session_neighborhood=$fl_wp_options['neighborhood'];}
	 if(!empty ($_GET['type'])){$session_type=$_GET['type'];}else{$session_type=$fl_wp_options['type'];}
	 
	$DDF_ACCESSOR = new DDFAccessor;
	$DDF_ACCESSOR->setRetsUrl('###.###.###.###/destination/place/national/');
	$DDF_ACCESSOR->setUserName($fl_wp_options['username']);
	$DDF_ACCESSOR->setPassword($fl_wp_options['password']);
	$DDF_ACCESSOR->setDestination($fl_wp_options['fl_account']);
	$DDF_ACCESSOR->setProvince($session_province);
	$DDF_ACCESSOR->setCity($session_city);
	$DDF_ACCESSOR->setAgentId($session_agent_id);
	$DDF_ACCESSOR->setBrokerageId($session_brokerage_id);
	$DDF_ACCESSOR->setMinPrice($session_min_price); 
	$DDF_ACCESSOR->setMaxPrice($session_max_price);
	$DDF_ACCESSOR->setMinBaths($session_min_baths);
	$DDF_ACCESSOR->setMinBeds($session_min_beds);
	$DDF_ACCESSOR->setNeighborhood($session_neighborhood);
	$DDF_ACCESSOR->setType($session_type);
	$listings = $DDF_ACCESSOR->ddf_http_connect();
	return $listings;
}
?>		