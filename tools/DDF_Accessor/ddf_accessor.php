<?php
/* Base class for ddf data access configuration.
Contributors: Dago Agacino
Version: 2.0.0
Author: Firstlook Media.
Author URI: www.myfirstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) */
require_once('ddf_access_config.php');
// this class contain methods to connect to the DDF mirror.
class DDFAccessor extends DataAccessConfig {	
	//function to access ddf web service from wordpress
	Public function ddf_wp_connect(){
		
		$args = array(
		    'timeout'     => 20,
		    'redirection' => 20,
		    'httpversion' => '1.0',
		    'user-agent'  => 'WordPress/4.3.1;' . get_bloginfo( 'url' ),
		    'blocking'    => true,
		    'headers' 	  => array('Authorization' => 'Basic ' . base64_encode($this->getCredential())),
		    'cookies'     => array(),
		    'body'        => null,
		    'compress'    => true,
		    'decompress'  => true,
		    'sslverify'   => false,
		    'stream'      => false,
		    'filename'    => null);
			$connection_Strig='https://'.$this->getConnectString().$this->getQueryParams();

		$response = wp_remote_get($connection_Strig ,$args );

		if( is_array($response) ) {
		
		  $header = $response['headers']; // array of http header lines
		  $body = $response['body']; // use the content
	  	
		if (!isset($body)){
			echo "<br>cURL Error; Failed to connect to 104.130.9.10: Network is unreachable \n";
			return false;
		}
		else{
			return $agentsListings=json_decode($body,true, 512);}
	  	
		}
	}
	//function to access ddf service as stand along 
	public function ddf_http_connect(){	
		//return $message=$this->getConnectString().$this->getQueryParams().'<br>';;
			try {	
			$url_string=$this->getConnectString().$this->getQueryParams();
			$ch = curl_init($url_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
			curl_setopt($ch, CURLAUTH_BASIC,CURLOPT_HTTPAUTH);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_USERPWD, $this->getCredential());
			curl_setopt($ch, CURLOPT_HEADER, false);
			//curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);
			$data = curl_exec($ch);
			$contents = utf8_encode($data); 
			$curl_errno = curl_errno($ch);
			$curl_error = curl_error($ch);
			curl_close($ch);
			      
			if ($curl_errno > 0) {
				echo "<br>HTTP Error (1): $curl_error\n";
				return false;
			} else {	
				$agentsListings=json_decode($data,true, 512);	  
				return $agentsListings;
			}
		}
		catch( Exception $e ) {
			print_r('<br>'.$e);
		}
		
	}
}
		
?>