<?php
/* Base class for ddf data access configuration.
Contributors: Dago Agacino
Version: 2.0.0
Author: Firstlook Media.
Author URI: www.myfirstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) */
abstract class DDFQuery{
	protected $province;
	protected $city;
	protected $minPrice; 
	protected $maxPrice;
	protected $minBaths;
	protected $minBeds;
	protected $neighborhood;
	protected $type;
	protected $postal;
	protected $agentid;
	protected $brokerageid;
public function setProvince( $prov ) {
	$this->province= $prov;
}
public function getProvince() {
	return $this->province;
}

public function setCity( $city ) {$this->city = $city;}
public function getCity() {return $this->city;}

public function setAgentId( $agentid ) {$this->agentid = $agentid;}
public function getAgentId() {return $this->agentid;}

public function setBrokerageId( $brokerageid ) {$this->brokerageid = $brokerageid;}
public function getBrokerageId() {return $this->brokerageid;}

public function setMinPrice( $minprice ) {$this->minPrice = $minprice;}
public function getMinPrice() {return $this->minPrice;}

public function setMaxPrice( $maxprice ) {$this->maxPrice = $maxprice;}
public function getMaxPrice() {return $this->maxPrice;}

public function setMinBeds( $beds ) {$this->minBeds = $beds;}
public function getMinBeds() {return $this->minBeds;}

public function setMinBaths($baths) {$this->minBaths=$baths;}
public function getMinBaths() {return $this->minBaths;}

public function getNeighborhood() {return $this->minBaths;}
public function setNeighborhood($neigh) {$this->neighborhood=$neigh;}	

public function getType() {return $this->type;}
public function setType($type) {$this->type=$type;}

public function getPostal() {return $this->postal;}
public function setPostal($post) {$this->postal=$post;}	

public function getQueryParams() {
	$searchQuery='?';
	$params=array(	
		'province'     =>$this->province,
		'city'         =>$this->city,
		'agentId'      =>$this->agentid,
		'brokerageId'  =>$this->brokerageid,
		'neighborhood' =>$this->neighborhood,
		'minPrice'     =>$this->minPrice,
		'maxPrice'     =>$this->maxPrice,
		'minBaths'     =>$this->minBaths,
		'minBeds'      =>$this->minBeds,
		'type'         =>$this->type,
		'postal'       =>$this->postal);
	foreach($params as $key=>$value){
		if(!empty($value)){
		$searchQuery.='&'.$key.'='.$value;	
		}
	}
	
	 $searchQuery= str_replace('?&','?',$searchQuery);
	 
	return (str_replace(' ','%20',$searchQuery));	
}}
?>