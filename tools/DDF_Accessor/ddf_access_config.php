<?php
/* Base class for ddf data access configuration.
Contributors: Dago Agacino
Version: 2.0.0
Author: Firstlook Media.
Author URI: www.myfirstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) */
require_once('ddf_query_config.php');
abstract class DataAccessConfig extends DDFQuery{
	protected $rets_login_url;
	protected $rets_username;
	protected $rets_password ; 
	protected $rets_destination;

abstract protected function ddf_wp_connect();
abstract protected function ddf_http_connect();
	
	public function setRetsUrl( $url ) {
		$this->rets_login_url= $url;
	}
	public function getRetsUrl() {
		return $this->rets_login_url;
	}
	
	public function setUserName( $user ) {
		$this->rets_username = $user;
	}
	public function getUserName() {
		return $this->rets_username;
	}
	
	public function setPassword( $pwd ) {
		$this->rets_password = $pwd;
	}
	public function getPassword() {
		return $this->rets_password;
	}
	
	public function setDestination( $dest ) {
		$this->rets_destination = $dest;
	}
	public function getDestination() {
		return $this->rets_destination;
	}
	public function getConnectString() {
		return $this->rets_login_url. $this->rets_destination;
	}
	public function getCredential() {
		return $this->rets_username.':'.$this->rets_password;
	}

}
				
?>