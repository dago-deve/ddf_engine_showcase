<?php
class DDF_SETTINGS{
    /**
     * Holds the values to be used in the fields callbacks
     */
    
	private $options;
    /**
     * Start up
     */
	public function set_options($options){
		$this->options=$options;
	}
    public function __construct(){
	 
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
		
    }
    /**
     * Add options page
     */
    public function add_plugin_page(){
        // This page will be under "Settings"	
        add_options_page( 'MLS Administration Settings', 'MLS Settings', 'manage_options', 'fl_ddf_menu',  array( $this, 'create_admin_page' )
        );
    }
    /**
     * Options page callback
     */
    public function create_admin_page(){
        // Set class property
        //$this->options = $fl_wp_options;
        ?>
        <div class="wrap">
			<div style="float: right; margin-right: 100px;">
		</div>
            <h2>DDF Conmtrol Panel</h2>           
            <form method="post" action="options.php">
            <?php
			echo '<div style="float: right; margin-right: 100px;">';
            submit_button(); 
			echo '</div>';
                // This prints out all hidden setting fields
                settings_fields( 'fl_general_option_group' ); 
				  
                do_settings_sections( 'fl-setting-admin' );
				
				echo '<div style="float: right; margin-right: 100px;">';
                submit_button(); 
				echo '</div>';
            ?>
            </form>
        </div>
        <?php
    }
    /**
     * Register and add settings
     */
    public function page_init(){
		$this->options=get_option('fl_general');
        register_setting('fl_general_option_group','fl_general',array( $this, 'sanitize' ) );
	//*****************DDF ACCESS************************
	//***************************************************
	        add_settings_section('agent_settings','<br>Agent Settings',array( $this, 'print_agent_settings_info' ), 'fl-setting-admin');
	//___________________________________________________
	        add_settings_field('agent_name','Agent Name',array( $this, 'name_display' ),'fl-setting-admin','agent_settings');      
	        add_settings_field('agent_id','Account',array( $this, 'fl_account_display' ),'fl-setting-admin','agent_settings');      
	//***************************************************
	//*****************CREDENTIALS*********************
	        add_settings_section('ddf_settings','<hr>Display Settings', array( $this, 'print_credential_settings_info' ),'fl-setting-admin');
	//___________________________________________________
	        add_settings_field('username','User Name',array( $this, 'username_display' ),'fl-setting-admin','ddf_settings' );       
	        add_settings_field('password','Password',array( $this, 'password_display' ),'fl-setting-admin','ddf_settings');  
	//***************************************************   
	//******************LAYOUT**********************
	        add_settings_section('display_settings','<hr>Search Settings', array( $this, 'print_layout_info' ),'fl-setting-admin');
	//___________________________________________________
	        add_settings_field('layout','Default layout',array( $this, 'layout_display' ),'fl-setting-admin','display_settings' );       
	        add_settings_field('search_box','Display Search Box',array( $this, 'search_box_display' ),'fl-setting-admin','display_settings'); 
			add_settings_field('details_mode','Display Options',array( $this, 'details_mode_display' ),'fl-setting-admin','display_settings'); 
	//***************************************************  
	//***************************************************
	        add_settings_section('search_settings','<hr>Default Search Settings<br>', array( $this, 'print_search_info' ),'fl-setting-admin');
	//___________________________________________________
	        add_settings_field('search','',array( $this, 'search_display' ),'fl-setting-admin','search_settings' );       

	//***************************************************  

    }	
    /** 
     * Print the Section text
     */
    public function print_section_info(){echo '<h5>Agent Information:</h5>';}
	public function print_agent_settings_info(){ echo '<h5>Agent Information:</h5>';}
	public function print_credential_settings_info(){ echo '<h5>Credential Information:</h5>';}
    public function print_layout_info(){ echo '<h5>Layout Settings:</h5>';}
	public function print_search_info(){ echo '<h5>Search Information:</h5>';}	
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input ){
		
        $new_input = array();
		
		if( isset( $input['agent_name'] ) ) $new_input['agent_name'] = sanitize_text_field( $input['agent_name'] );
				
        if( isset( $input['fl_account'] ) ) $new_input['fl_account'] = sanitize_text_field( $input['fl_account'] );
		
        if( isset( $input['username'] ) ) $new_input['username'] = sanitize_text_field( $input['username'] );
		
        if( isset( $input['password'] ) ) $new_input['password'] = sanitize_text_field( $input['password'] );
				
        if( isset( $input['layout'] ) ) $new_input['layout'] = sanitize_option('value',$input['layout']);
		
        if( isset( $input['search_box'] ) ) $new_input['search_box'] = sanitize_option('value',$input['search_box']);
		
		if( isset( $input['details_mode'] ) ) $new_input['details_mode'] = sanitize_text_field( $input['details_mode'] );
		
        if( isset( $input['province'] ) ) $new_input['province'] = sanitize_text_field( $input['province'] );
		
        if( isset( $input['city'] ) ) $new_input['city'] = sanitize_text_field( $input['city'] );
		
		if( isset( $input['min_price'] ) ) $new_input['min_price'] = sanitize_text_field( $input['min_price'] );
		
		if( isset( $input['max_price'] ) ) $new_input['max_price'] = sanitize_text_field( $input['max_price'] );
		
		if( isset( $input['min_baths'] ) ) $new_input['min_baths'] = sanitize_text_field( $input['min_baths'] );
		
		if( isset( $input['min_beds'] ) ) $new_input['min_beds'] = sanitize_text_field( $input['min_beds'] );
		
		if( isset( $input['type'] ) ) $new_input['type'] = sanitize_text_field( $input['type'] );
		
		if( isset( $input['neighborhood'] ) ) $new_input['neighborhood'] = sanitize_text_field( $input['neighborhood'] );
			
		
		$fl_wp_options=$new_input;
			
        return $new_input;

    }	
	/** 
     * Get the settings option array and print one of its values
     */
    public function name_display(){
	
       echo '<input style="width:300px;" type="text" id="agent_name" name="fl_general[agent_name]" value="'.$this->options['agent_name'].'" /></div>';
	   
	   
    }
	public function fl_account_display(){
       echo'<input style="width:300px;" type="text" id="fl_account" name="fl_general[fl_account]" value="'.$this->options['fl_account'].'" />';

    }	
    public function username_display(){
        echo'<input style="width:300px;" type="text" id="username" name="fl_general[username]" value="'.$this->options['username'].'" />';
    }
    public function password_display(){
       echo'<input style="width:300px;" type="text" id="password" name="fl_general[password]" value="'.$this->options['password'].'" />';
    }
    public function layout_display(){
		//must be a better way to write this logic; !!! for now is fine.
		if (!isset($this->options['layout'])){$this->options['layout']='property_card';}
		switch ($this->options['layout']) {
			case 'property_card':
	        echo'<input  type="radio" id="layout" name="fl_general[layout]" checked value="property_card"/>Property Card Layout';
	        echo'<input  type="radio" id="layout" name="fl_general[layout]"  value="slide_show"/>Slide Show Layout';
			break;
			case 'slide_show':
	        echo'<input  type="radio" id="layout" name="fl_general[layout]" value="property_card"/>Property Card Layout';
	        echo'<input  type="radio" id="layout" name="fl_general[layout]" checked value="slide_show"/>Slide Show Layout';
			break;

		}

    }
 	public function search_box_display() {
		if ($this->options['search_box']=='on'){
		echo'<div class="switch"><input class="" type="checkbox" id="search_box" name="fl_general[search_box] value="ckecked"  checked /><label for="search_box">Display Search Box</label></div>';
		}else{     echo'<div class="switch"><input class="" type="checkbox" id="search_box" name="fl_general[search_box] value="ckecked"/><label for="search_box">Display Search Box</label></div>';}
	}
    public function details_mode_display(){
	  
	 echo '<select style="width:300px;" id="details_mode" name="fl_general[details_mode]"  style="height: 50px;"><option value="'.$this->options['details_mode'].'">'.$this->options['details_mode'].'</option><option value="full">Full View</option><option value="large">Dialog View</option></select>';	      
	
		
    }	
	public function search_display(){

	    include_once(WP_PLUGIN_DIR.'/FL_DDF_Engine/components/admin_searchBox.php');

	}

}
?>