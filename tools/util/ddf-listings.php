<?php
/* Util for listings data manipukation.
Contributors: Dago Agacino
Version: 2.0.0
Author: Firstlook Media.
Author URI: www.firstlook.ca
License: pending...
Copyright 2015 FirstLook Media  (email : support@firstlook.ca) 
Alternative sorting fields
[bedrooms],[bathrooms],[flooring],[lastUpdated],[price],[propertyKey],[yearBuilt],[status],[lotSize]
*/
include( WP_PLUGIN_DIR.'/FL_DDF_Engine/tools/DDF_Accessor/ddf_data_management.php');

function fl_listings_sort($by,$order){	
	$fl_wp_options=get_option('fl_general');
	$agentsListings=ddf_connect();

	$sorted_list='';
	$sorted_listings='';
	$page_lists='';
	if( $agentsListings==false){
	return false;
	}else{
		//Sort List
		foreach($agentsListings as $key=>$value){

			if(is_array($sorted_list)){
				array_push($sorted_list,$value['tombstone'][$by]);
			}elseif(!is_array($sorted_list)){
				$sorted_list=array($value['tombstone'][$by]);
			}
		}
		asort($sorted_list,SORT_NUMERIC);
		foreach($sorted_list as $key=>$value){
			
			if(is_array($sorted_listings) ){
				array_push($sorted_listings,$agentsListings[$key]);
			}elseif(!is_array($sorted_listings)){
				$sorted_listings=array($agentsListings[$key]);
			}
		}
	
		foreach($sorted_listings as $list){
			if(!($list['listingInfo']['mlsNumber']=='')){
			
				if(is_array($page_lists)){
					array_push($page_lists,$list);
				}elseif(!is_array($page_lists)){
					$page_lists=$list;
				}
			}
		
		
		}
		//SORT_REGULAR,SORT_NUMERIC,SORT_STRING,SORT_LOCALE_STRING,SORT_NATURAL,SORT_FLAG_CASE
		//array_multisort($sorted_listings['tombstone']['price'], SORT_ASC, SORT_STRING);
			switch ($order) {
				case 'hl':
					# Order high to low
					ksort($sorted_listings,SORT_NUMERIC);	
					return $sorted_listings;	

					break;
				case 'lh':
					# low to high 
					krsort($sorted_listings,SORT_NUMERIC);
					return $sorted_listings;	
					break;
				}
		}
	}
		
?>

