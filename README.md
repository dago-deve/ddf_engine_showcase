#FirstLook Media Solutions DDF Engine technical documentation#
##DDF Engine technical documentation##
###Development Team:###
Dago Agacino


##Table of content##
Introduction 
System Overview 
Design Considerations 
Goals and Guidelines 
Dependencies 
Retrieve data 
Format outputs Interface
Detailed System Design
Sumary


##Introduction##
This document is designed to be a reference for any person wishing to implement or any person interested in the architecture of the DDF Search plugin, ddf plugin client application. This document describes each application’s architecture and sub- architecture their associated interfaces, and the motivations behind the chosen design. Both high-level and low-level designs are included in this document.

This document should be read by an individual with a technical background and has experience reading data flow diagrams (DFDs), control flow diagrams (CFDs), interface designs, and development experience in object oriented programming and event driven programming.

This design document has an accompanying specification document and test document. This design document is per DDF Search Engine Specification version 0.3.3.5 Any previous or later revisions of the specifications require a different revision of this design document.

This document includes but is not limited to the following information for the ddf plugin; system overview, design considerations, architectural strategies, system architecture and detailed system design.

##System Overview##
￼###Design Considerations###
This section describes many of the issues that needed to be addressed or resolved before attempting to devise a complete design solution.
Assumptions and Dependencies

This plugin was .......... the Firstlook mirror-1 server.

This client application required an HTTP connection to the server, and needs specific parameters to connect to the server side.
￼￼￼￼
Server IPaddress ###.###.###.###
Path to resource /resource/path/way/
Destination ID Depends on specific user all test were done using (###)
Username **********
Password **********
￼￼￼￼￼￼￼￼￼
##Goals and Guidelines##
The major goal of the DDF search client is that it be extremely simple and intuitive to use. The application is geared towards the realtors, not a technically inclined individual. It is very important that the prompts for the user be clear and concise since this will be the highest level of interaction between the application and the user. It is also important that series of prompts and responses be tested with users before being deployed as part of the product so that all interaction is “approved” by a potential user.

The second major goal of the application is that the user gets a response in a timely fashion. Intuition tells that a user will lose interest if they have to wait long times for software to respond. This is why the design has minimal data transferred between client and server. In this design, a minimum set of information is transferred to the server in order to retrieve the necessary information, and the server only returns the a pre defined amount of data that is then formatted into a readable phrase on the client side.

This design attempted to keep the client application as data independent as possible. All prompts and responses on the client side are completely data driven, so any response can be changed by simple change the criteria without changing any code. This makes the client capable of prompting and responding to any structural type of data.

##Dependencies##
This application uses some third party software, and is directly related to the version of this softwares package with in.
• JavaScript
• Bootstrap CSS framework • PHP ver. 5.6

### Retrieve data ###
We’ll first retrieve data from DDF-MIRROR (http://###.###.###.###/resource/path/ national/) if the data retrieved OK, then use this data as the input of the parse subroutine. Otherwise, log errors.
We go to each field and remap the input to match a standard array, that can be later manipulated and displayed to the user. the input data is collected as json and imported.
raw input sample :
DDF Engine technical documentation

```
#!html

[{
  "walkinCloset": {
"size": "",
    "level": ""
  },
  "bedrooms": {
    "masterBed": {
"size": "",
      "level": ""
    },
    "bedroom2": {
      "size": "",
      "level": ""
    },
    "bedroom3": {
      "size": "",
      "level": ""
    },
    "bedroom4": {
      "size": "",
"level": "" }
  },
  "tombstone": {
    "bedrooms": 0,
    "flooring": "",
    "lastUpdated": "2015-06-05T01:19:52",
    "fullDescription": "",
    "price": 0.0,
    "utilities": "",
    "heating": "",
    "propertyKey": "15769687",
    "taxes": "",
    "yearBuilt": "1987",
    "status": "Active",
    "lotSize": "0",
    "bathrooms": 0,
    "exclusions": "",
    "remarks": "This corner unit has large entry way (not a bay) with an area
for main floor office with half bath.  A large area for storage with 20 foot
ceilings or possibility of adding to the second floor. Upstairs has a
kitchenette and full bath with a sitting area great for a staff room or
overnight person.  Please call today for more information. [A]",
    "parking": "",
    "inclusions": "",
    "associationFees": ""
  },
  "storageRoom": {
"size": "",
    "level": ""
  },
  "livingRoom": {
    "size": "",
    "level": ""
  },


```


###Format outputs:###

Put parsed data in the format that we’ll discuss in the interface section. Then check if we get the correct data. The reason that we wait until this part to check the data instead of doing that right after we get the data is efficiency. We don’t want spending too much time checking data. If the data is correct, then write it to file. Otherwise, log errors.

```
#!html

[0] => Array (
[tombstone] => Array (
[bedrooms] => 3
[flooring] =>
[lastUpdated] => 2016-01-29T19:08:20
[fullDescription] =>
[price] => 205000
[utilities] =>
[heating] => Forced air
[propertyKey] => 16539713
[taxes] =>
[yearBuilt] =>
[status] => Active
[lotSize] => 0
[bathrooms] => 2
[exclusions] =>
[remarks] => Large Open Balcony. Well Kept Apartment Steps From Public Transit & Minutes
From Schools, Shopping, & Place Of Worship. This Building Is Well Cared For With A Renovated Lobby & A New Roof. Great Value For Your Money! **** EXTRAS **** Fridge, Stove, & Light Fixtures.
[parking] => [inclusions] => [associationFees] =>
)
[location] => Array (
[community] =>
[city] => Toronto
[latitude] => 0
[neighborhood] => Dorset Park [zoning] =>
[propertyType] => Single Family [address2] =>
[longitude] => 0
[postalCode] => M1P4V3 [latLongAccuracy] => 0 [province] => Ontario
[address] => #1012 - 301 PRUDENTIAL DR [directions] =>
DDF Engine technical documentation


)

[listingInfo] => Array (
[listAgentCode1] => 1683720
[mlsNumber] => E3404006
[brokerageId] => SUTTON GROUP-ADMIRAL REALTY INC. [listAgentCode3] =>
[brokerageCode] =>
[listAgentCode2] =>
)
[rental] => Array (
[amenities] => [rentalPrice] => [tenantPays] =>
)
[details] => Array (
[fireplacFuel] =>
[style] =>
[dwellingType] => Single Family [parkingDesc] => [numGarageSpace] => [heatingFuel] => Natural gas [taxYear] =>
[extras] =>
[acDesc] => Central air conditioning
)
[dates] => Array (
[lastModifiedDate] => 2016-01-29T19:08:20 )
[condo] => Array (
[condoFeeIncludes] =>
[condoFee] => )
[marketing] => Array (
[tagLine] => [fullDescription] => [virtualTour] =>
)
[rooms] => Array (
DDF Engine technical documentation


[masterBed] => Array (
[size] => 4.75 m X
[level] => Flat )
[bedroom2] => Array (
[size] => 3.73 m X
[level] => Flat )
[bedroom3] => Array (
[size] => 3.72 m X
[level] => Flat )
[bedroom4] => Array (
[size] =>
[level] => )
[walkinCloset] => Array (
[size] =>
[level] => )
[livingRoom] => Array (
[size] => 6.49 m X
[level] => Flat )
[fullBath] => Array (
[size] =>
[level] => )
[kitchen] => Array (
[size] => 4.51 m X
[level] => Flat )
[diningRoom] => Array (
[size] => 3.72 m X [level] => Flat
3.26 m
2.66 m
3.05 m
3.3 m
2.41 m
2.85 m
)

[familyRoom] => Array (
[size] =>
[level] => )
[storageRoom] => Array (
[size] =>
[level] => )
[sunRoom] => Array (
[size] =>
[level] => )
[powderRoom] => Array (
[size] =>
[level] => )
[den] => Array (
[size] =>
[level] => )
)
[photos] => Array (
[0] => Array (
[description] => null
[id] => 56b206a40f00000f009a6fc0 [contentType] => image/jpeg
[fileUrl] => http://3993e8bd2493b955cfb7-
aaf1733fee3f5ce66c7d3c6d069d071a.r70.cf5.rackcdn.com/16539713_1.jpeg [objectType] => LargePhoto
) )
[contentId] => 16539713
[objectId] => 1 )


```

##Detailed System Design ##

###1 - DDF Access Config###
Classification
Abstract class.

Purpose
This class implements the necessary settings, and functions to conform the connection string
information from the HTTP call. This class is used as part of the client application.

Uses/Interactions
This module is to be used by the client ddf_query_config. This subsystem will be invoked when the a connection request is made.

###Functions###


###Functions###

##3 - DDF accessor## 
Classification
Class.



Prompts will be defined as the point at which the computer and the user interact. These are decision points in the control flow of the program, allowing the program to branch based on the user response.
Prompt Text is the text that is read by the computer to the user at any given prompt. There may be several levels of prompt text for any prompt; the actual prompt text read to the user depends on the current user level.

Commands are the legal responses the user may make at any given prompt.
Scripts are series of prompts that are executed in succession. They may be used when multiple pieces of information must be determined in order to form a query string.